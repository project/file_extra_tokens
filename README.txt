Provides file object tokens from URI or URL tokens.

Requires File Entity module.

Partially requires https://www.drupal.org/node/1905818
